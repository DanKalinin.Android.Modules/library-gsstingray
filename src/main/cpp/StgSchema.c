//
// Created by Dan on 17.11.2021.
//

#include "StgSchema.h"










StgChannelType StgChannel = {0};

void StgChannelLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsstingray/StgChannel");
    StgChannel.class = (*env)->NewGlobalRef(env, class);
    StgChannel.channelListId = (*env)->GetFieldID(env, class, "channelListId", "Ljava/lang/String;");
    StgChannel.channelNumber = (*env)->GetFieldID(env, class, "channelNumber", "I");
    StgChannel.channelListName = (*env)->GetFieldID(env, class, "channelListName", "Ljava/lang/String;");
    StgChannel.channelName = (*env)->GetFieldID(env, class, "channelName", "Ljava/lang/String;");
    StgChannel.visible = (*env)->GetFieldID(env, class, "visible", "Z");
    StgChannel.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void StgChannelUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, StgChannel.class);
}

jobject StgChannelFrom(JNIEnv *env, STGChannel *sdkChannel) {
    jstring channelListId = (*env)->NewStringUTF(env, sdkChannel->channel_list_id);
    jstring channelListName = (*env)->NewStringUTF(env, sdkChannel->channel_list_name);
    jstring channelName = (*env)->NewStringUTF(env, sdkChannel->channel_name);

    jobject this = (*env)->NewObject(env, StgChannel.class, StgChannel.init);
    (*env)->SetObjectField(env, this, StgChannel.channelListId, channelListId);
    (*env)->SetIntField(env, this, StgChannel.channelNumber, sdkChannel->channel_number);
    (*env)->SetObjectField(env, this, StgChannel.channelListName, channelListName);
    (*env)->SetObjectField(env, this, StgChannel.channelName, channelName);
    (*env)->SetBooleanField(env, this, StgChannel.visible, sdkChannel->visible);
    return this;
}

STGChannel *StgChannelTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring channelListId = (*env)->GetObjectField(env, this, StgChannel.channelListId);
    jstring channelListName = (*env)->GetObjectField(env, this, StgChannel.channelListName);
    jstring channelName = (*env)->GetObjectField(env, this, StgChannel.channelName);

    STGChannel sdkChannel = {0};
    sdkChannel.channel_list_id = GeJNIEnvGetStringUTFChars(env, channelListId, NULL);
    sdkChannel.channel_number = (*env)->GetIntField(env, this, StgChannel.channelNumber);
    sdkChannel.channel_list_name = GeJNIEnvGetStringUTFChars(env, channelListName, NULL);
    sdkChannel.channel_name = GeJNIEnvGetStringUTFChars(env, channelName, NULL);
    sdkChannel.visible = (*env)->GetBooleanField(env, this, StgChannel.visible);
    STGChannel *ret = stg_channel_copy(&sdkChannel);

    GeJNIEnvReleaseStringUTFChars(env, channelListId, sdkChannel.channel_list_id);
    GeJNIEnvReleaseStringUTFChars(env, channelListName, sdkChannel.channel_list_name);
    GeJNIEnvReleaseStringUTFChars(env, channelName, sdkChannel.channel_name);

    return ret;
}

jobjectArray StgChannelsFrom(JNIEnv *env, GList *sdkChannels) {
    guint n = g_list_length(sdkChannels);
    jobjectArray this = (*env)->NewObjectArray(env, n, StgChannel.class, NULL);
    jsize i = 0;

    for (GList *sdkChannel = sdkChannels; sdkChannel != NULL; sdkChannel = sdkChannel->next) {
        jobject channel = StgChannelFrom(env, sdkChannel->data);
        (*env)->SetObjectArrayElement(env, this, i, channel);
        i++;
    }

    return this;
}










StgEndpointType StgEndpoint = {0};

void StgEndpointLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsstingray/StgEndpoint");
    StgEndpoint.class = (*env)->NewGlobalRef(env, class);
    StgEndpoint.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    StgEndpoint.ip = (*env)->GetFieldID(env, class, "ip", "Ljava/lang/String;");
    StgEndpoint.port = (*env)->GetFieldID(env, class, "port", "I");
    StgEndpoint.model = (*env)->GetFieldID(env, class, "model", "Ljava/lang/String;");
    StgEndpoint.version = (*env)->GetFieldID(env, class, "version", "Ljava/lang/String;");
    StgEndpoint.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void StgEndpointUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, StgEndpoint.class);
}

jobject StgEndpointFrom(JNIEnv *env, STGEndpoint *sdkEndpoint) {
    jstring name = (*env)->NewStringUTF(env, sdkEndpoint->name);
    jstring ip = (*env)->NewStringUTF(env, sdkEndpoint->ip);
    jstring model = (*env)->NewStringUTF(env, sdkEndpoint->model);
    jstring version = (*env)->NewStringUTF(env, sdkEndpoint->version);

    jobject this = (*env)->NewObject(env, StgEndpoint.class, StgEndpoint.init);
    (*env)->SetObjectField(env, this, StgEndpoint.name, name);
    (*env)->SetObjectField(env, this, StgEndpoint.ip, ip);
    (*env)->SetIntField(env, this, StgEndpoint.port, sdkEndpoint->port);
    (*env)->SetObjectField(env, this, StgEndpoint.model, model);
    (*env)->SetObjectField(env, this, StgEndpoint.version, version);
    return this;
}

STGEndpoint *StgEndpointTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring name = (*env)->GetObjectField(env, this, StgEndpoint.name);
    jstring ip = (*env)->GetObjectField(env, this, StgEndpoint.ip);
    jstring model = (*env)->GetObjectField(env, this, StgEndpoint.model);
    jstring version = (*env)->GetObjectField(env, this, StgEndpoint.version);

    STGEndpoint sdkEndpoint = {0};
    sdkEndpoint.name = GeJNIEnvGetStringUTFChars(env, name, NULL);
    sdkEndpoint.ip = GeJNIEnvGetStringUTFChars(env, ip, NULL);
    sdkEndpoint.port = (*env)->GetIntField(env, this, StgEndpoint.port);
    sdkEndpoint.model = GeJNIEnvGetStringUTFChars(env, model, NULL);
    sdkEndpoint.version = GeJNIEnvGetStringUTFChars(env, version, NULL);
    STGEndpoint *ret = stg_endpoint_copy(&sdkEndpoint);

    GeJNIEnvReleaseStringUTFChars(env, name, sdkEndpoint.name);
    GeJNIEnvReleaseStringUTFChars(env, ip, sdkEndpoint.ip);
    GeJNIEnvReleaseStringUTFChars(env, model, sdkEndpoint.model);
    GeJNIEnvReleaseStringUTFChars(env, version, sdkEndpoint.version);

    return ret;
}
