//
// Created by Dan on 17.11.2021.
//

#ifndef LIBRARY_GSSTINGRAY_STGCLIENT_H
#define LIBRARY_GSSTINGRAY_STGCLIENT_H

#include "StgMain.h"
#include "StgSchema.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID base;
    jfieldID userAgent;
    jmethodID init;
} StgClientType;

extern StgClientType StgClient;

void StgClientLoad(JNIEnv *env);
void StgClientUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_GSSTINGRAY_STGCLIENT_H
