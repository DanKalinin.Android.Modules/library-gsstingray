//
// Created by Dan on 17.11.2021.
//

#ifndef LIBRARY_GSSTINGRAY_STGMAIN_H
#define LIBRARY_GSSTINGRAY_STGMAIN_H

#include <gs-stingray/gs-stingray.h>
#include <GlibExt.h>
#include <SoupExt.h>
#include <GnuTLSExt.h>
#include <GLibNetworkingExt.h>
#include <JsonExt.h>
#include <AvahiExt.h>

#endif //LIBRARY_GSSTINGRAY_STGMAIN_H
