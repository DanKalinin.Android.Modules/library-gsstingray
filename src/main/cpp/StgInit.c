//
// Created by Dan on 17.11.2021.
//

#include "StgInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    stg_init();

    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    StgChannelLoad(env);
    StgEndpointLoad(env);
    StgClientLoad(env);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    StgChannelUnload(env);
    StgEndpointUnload(env);
    StgClientUnload(env);
}
