//
// Created by Dan on 17.11.2021.
//

#include "StgClient.h"

StgClientType StgClient = {0};

void StgClientLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/gsstingray/StgClient");
    StgClient.class = (*env)->NewGlobalRef(env, class);
    StgClient.object = (*env)->GetFieldID(env, class, "object", "J");
    StgClient.base = (*env)->GetFieldID(env, class, "base", "Ljava/lang/String;");
    StgClient.userAgent = (*env)->GetFieldID(env, class, "userAgent", "Ljava/lang/String;");
    StgClient.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void StgClientUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, StgClient.class);
}

JNIEXPORT void JNICALL Java_library_gsstingray_StgClient_finalize(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, StgClient.object);
    g_object_unref(GSIZE_TO_POINTER(object));
}

JNIEXPORT jobject JNICALL Java_library_gsstingray_StgClient_client(JNIEnv *env, jclass class, jstring base, jstring userAgent) {
    gchar *sdkBase = GeJNIEnvGetStringUTFChars(env, base, NULL);
    g_autoptr(SoupURI) sdkBase1 = soup_uri_new(sdkBase);
    GeJNIEnvReleaseStringUTFChars(env, base, sdkBase);

    gchar *sdkUserAgent = GeJNIEnvGetStringUTFChars(env, userAgent, NULL);
    STGSOESession *object = stg_soe_session_new(sdkBase1, sdkUserAgent);
    GeJNIEnvReleaseStringUTFChars(env, userAgent, sdkUserAgent);

    jobject this = (*env)->NewObject(env, class, StgClient.init);
    (*env)->SetLongField(env, this, StgClient.object, GPOINTER_TO_SIZE(object));
    (*env)->SetObjectField(env, this, StgClient.base, base);
    (*env)->SetObjectField(env, this, StgClient.userAgent, userAgent);
    return this;
}

JNIEXPORT void JNICALL Java_library_gsstingray_StgClient_abort(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, StgClient.object);
    soup_session_abort(GSIZE_TO_POINTER(object));
}

JNIEXPORT jboolean JNICALL Java_library_gsstingray_StgClient_powerSetSync(JNIEnv *env, jobject this, jboolean on) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, StgClient.object);
    g_autoptr(GError) sdkError = NULL;

    if (stg_soe_session_power_set_sync(GSIZE_TO_POINTER(object), on, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_gsstingray_StgClient_channelsGetSync(JNIEnv *env, jobject this) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, StgClient.object);
    g_autolist(STGChannel) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;

    if (stg_soe_session_channels_get_sync(GSIZE_TO_POINTER(object), &sdkRet, &sdkError)) {
        ret = StgChannelsFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_gsstingray_StgClient_channelSetSync(JNIEnv *env, jobject this, jobject channel) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, StgClient.object);
    g_autoptr(STGChannel) sdkChannel = StgChannelTo(env, channel);
    g_autoptr(GError) sdkError = NULL;

    if (stg_soe_session_channel_set_sync(GSIZE_TO_POINTER(object), sdkChannel, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}
