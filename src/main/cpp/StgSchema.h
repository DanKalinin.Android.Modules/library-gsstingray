//
// Created by Dan on 17.11.2021.
//

#ifndef LIBRARY_GSSTINGRAY_STGSCHEMA_H
#define LIBRARY_GSSTINGRAY_STGSCHEMA_H

#include "StgMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID channelListId;
    jfieldID channelNumber;
    jfieldID channelListName;
    jfieldID channelName;
    jfieldID visible;
    jmethodID init;
} StgChannelType;

extern StgChannelType StgChannel;

void StgChannelLoad(JNIEnv *env);
void StgChannelUnload(JNIEnv *env);

jobject StgChannelFrom(JNIEnv *env, STGChannel *sdkChannel);
STGChannel *StgChannelTo(JNIEnv *env, jobject this);

jobjectArray StgChannelsFrom(JNIEnv *env, GList *sdkChannels);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID name;
    jfieldID ip;
    jfieldID port;
    jfieldID model;
    jfieldID version;
    jmethodID init;
} StgEndpointType;

extern StgEndpointType StgEndpoint;

void StgEndpointLoad(JNIEnv *env);
void StgEndpointUnload(JNIEnv *env);

jobject StgEndpointFrom(JNIEnv *env, STGEndpoint *sdkEndpoint);
STGEndpoint *StgEndpointTo(JNIEnv *env, jobject this);

G_END_DECLS










#endif //LIBRARY_GSSTINGRAY_STGSCHEMA_H
