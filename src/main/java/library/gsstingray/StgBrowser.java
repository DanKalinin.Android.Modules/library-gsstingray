package library.gsstingray;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import library.java.lang.LjlObject;

public class StgBrowser extends LjlObject implements NsdManager.DiscoveryListener {
    public interface Listener {
        void endpointFound(StgBrowser sender, StgEndpoint endpoint);
        void endpointRemoved(StgBrowser sender, String name);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void endpointFound(StgBrowser sender, StgEndpoint endpoint) {
            for (Listener listener : this) {
                listener.endpointFound(sender, endpoint);
            }
        }

        @Override
        public void endpointRemoved(StgBrowser sender, String name) {
            for (Listener listener : this) {
                listener.endpointRemoved(sender, name);
            }
        }
    }

    public String type;
    public Listeners listeners;
    private NsdManager nsdManager;
    private boolean started;

    @Override
    public void onStartDiscoveryFailed(String serviceType, int errorCode) {

    }

    @Override
    public void onStopDiscoveryFailed(String serviceType, int errorCode) {

    }

    @Override
    public void onDiscoveryStarted(String serviceType) {

    }

    @Override
    public void onDiscoveryStopped(String serviceType) {

    }

    @Override
    public void onServiceFound(NsdServiceInfo serviceInfo) {
        StgResolver resolver = StgResolver.resolver(this);
        nsdManager.resolveService(serviceInfo, resolver);
    }

    @Override
    public void onServiceLost(NsdServiceInfo serviceInfo) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.endpointRemoved(this, serviceInfo.getServiceName()));
    }

    public static StgBrowser browser(Context context, String type) {
        StgBrowser ret = new StgBrowser();
        ret.type = type;
        ret.listeners = new Listeners();
        ret.nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        ret.started = false;
        return ret;
    }

    public void start() {
        if (started) return;
        nsdManager.discoverServices(type, NsdManager.PROTOCOL_DNS_SD, this);
        started = true;
    }

    public void stop() {
        if (!started) return;
        nsdManager.stopServiceDiscovery(this);
        started = false;
    }
}
