package library.gsstingray;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Handler;
import android.os.Looper;
import library.java.lang.LjlObject;

public class StgResolver extends LjlObject implements NsdManager.ResolveListener {
    StgBrowser browser;

    @Override
    public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {

    }

    @Override
    public void onServiceResolved(NsdServiceInfo serviceInfo) {
        byte[] model = serviceInfo.getAttributes().get("model");
        byte[] version = serviceInfo.getAttributes().get("txtvers");

        StgEndpoint endpoint = new StgEndpoint();
        endpoint.name = serviceInfo.getServiceName();
        endpoint.ip = serviceInfo.getHost().getHostAddress();
        endpoint.port = serviceInfo.getPort();
        endpoint.model = new String(model);
        endpoint.version = new String(version);

        new Handler(Looper.getMainLooper()).post(() -> browser.listeners.endpointFound(browser, endpoint));
    }

    public static StgResolver resolver(StgBrowser browser) {
        StgResolver ret = new StgResolver();
        ret.browser = browser;
        return ret;
    }
}
