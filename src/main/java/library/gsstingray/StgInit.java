package library.gsstingray;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import library.avahiext.AvhInit;
import library.glibext.GeInit;
import library.glibnetworkingext.GneInit;
import library.gnutlsext.TlsInit;
import library.java.lang.LjlObject;
import library.jsonext.JseInit;
import library.soupext.SoeInit;

public class StgInit extends LjlObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-gsstingray");
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        ret.add(GeInit.class);
        ret.add(SoeInit.class);
        ret.add(TlsInit.class);
        ret.add(GneInit.class);
        ret.add(JseInit.class);
        ret.add(AvhInit.class);
        return ret;
    }
}
