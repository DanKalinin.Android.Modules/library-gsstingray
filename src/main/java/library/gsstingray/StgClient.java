package library.gsstingray;

import android.os.Handler;
import android.os.Looper;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class StgClient extends LjlObject {
    private long object;
    public String base;
    public String userAgent;

    @Override
    protected native void finalize() throws Throwable;

    public static native StgClient client(String base, String userAgent);

    public static StgClient client(StgEndpoint endpoint, String userAgent) {
        String base = "http://" + endpoint.ip + ":" + endpoint.port;
        return StgClient.client(base, userAgent);
    }

    public native void abort();

    // PowerSet

    public native boolean powerSetSync(boolean on) throws GeException;

    public interface PowerSetAsyncCallback {
        void callback(GeException exception);
    }

    public void powerSetAsync(boolean on, PowerSetAsyncCallback callback) {
        new Thread(() -> {
            try {
                powerSetSync(on);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // ChannelsGet

    public native StgChannel[] channelsGetSync() throws GeException;

    public interface ChannelsGetAsyncCallback {
        void callback(StgChannel[] channels, GeException exception);
    }

    public void channelsGetAsync(ChannelsGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                StgChannel[] channels = channelsGetSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(channels, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // ChannelSet

    public native boolean channelSetSync(StgChannel channel) throws GeException;

    public interface ChannelSetAsyncCallback {
        void callback(GeException exception);
    }

    public void channelSetAsync(StgChannel channel, ChannelSetAsyncCallback callback) {
        new Thread(() -> {
            try {
                channelSetSync(channel);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
