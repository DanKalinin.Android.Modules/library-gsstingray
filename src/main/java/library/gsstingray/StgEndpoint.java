package library.gsstingray;

import library.java.lang.LjlObject;

public class StgEndpoint extends LjlObject {
    public String name;
    public String ip;
    public int port;
    public String model;
    public String version;
}
