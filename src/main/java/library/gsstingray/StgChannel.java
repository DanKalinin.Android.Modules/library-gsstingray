package library.gsstingray;

import library.java.lang.LjlObject;

public class StgChannel extends LjlObject {
    public String channelListId;
    public int channelNumber;
    public String channelListName;
    public String channelName;
    public boolean visible;
}
